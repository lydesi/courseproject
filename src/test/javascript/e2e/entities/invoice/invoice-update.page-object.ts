import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class InvoiceUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('saveCashBankApp.invoice.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  scoreInput: ElementFinder = element(by.css('input#invoice-score'));

  currencyUnitInput: ElementFinder = element(by.css('input#invoice-currencyUnit'));

  userSelect = element(by.css('select#invoice-user'));
}
