import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class PaymentUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('saveCashBankApp.payment.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  nameInput: ElementFinder = element(by.css('input#payment-name'));

  bankAccountNumberInput: ElementFinder = element(by.css('input#payment-bankAccountNumber'));

  scoreNumberInput: ElementFinder = element(by.css('input#payment-scoreNumber'));

  invoiceSelect = element(by.css('select#payment-invoice'));
}
