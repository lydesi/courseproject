import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class TransactionUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('saveCashBankApp.transaction.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  currencyUnitInput: ElementFinder = element(by.css('input#transaction-currencyUnit'));

  dateInput: ElementFinder = element(by.css('input#transaction-date'));

  invoiceSelect = element(by.css('select#transaction-invoice'));
}
