/* tslint:disable no-unused-expression */
import { browser } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import CardComponentsPage, { CardDeleteDialog } from './card.page-object';
import CardUpdatePage from './card-update.page-object';
import CardDetailsPage from './card-details.page-object';

import {
  clear,
  click,
  getRecordsCount,
  isVisible,
  selectLastOption,
  waitUntilAllDisplayed,
  waitUntilAnyDisplayed,
  waitUntilCount,
  waitUntilDisplayed,
  waitUntilHidden,
} from '../../util/utils';

const expect = chai.expect;

describe('Card e2e test', () => {
  let navBarPage: NavBarPage;
  let updatePage: CardUpdatePage;
  let detailsPage: CardDetailsPage;
  let listPage: CardComponentsPage;
  let deleteDialog: CardDeleteDialog;
  let beforeRecordsCount = 0;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    await navBarPage.login(username, password);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });

  it('should load Cards', async () => {
    await navBarPage.getEntityPage('card');
    listPage = new CardComponentsPage();

    await waitUntilAllDisplayed([listPage.title, listPage.footer]);

    expect(await listPage.title.getText()).not.to.be.empty;
    expect(await listPage.createButton.isEnabled()).to.be.true;

    await waitUntilAnyDisplayed([listPage.noRecords, listPage.table]);
    beforeRecordsCount = (await isVisible(listPage.noRecords)) ? 0 : await getRecordsCount(listPage.table);
  });
  describe('Create flow', () => {
    it('should load create Card page', async () => {
      await listPage.createButton.click();
      updatePage = new CardUpdatePage();

      await waitUntilAllDisplayed([updatePage.title, updatePage.footer, updatePage.saveButton]);

      expect(await updatePage.title.getAttribute('id')).to.match(/saveCashBankApp.card.home.createOrEditLabel/);
    });

    it('should create and save Cards', async () => {
      await updatePage.scoreInput.sendKeys('5');
      expect(await updatePage.scoreInput.getAttribute('value')).to.eq('5');

      await updatePage.numberInput.sendKeys('5');
      expect(await updatePage.numberInput.getAttribute('value')).to.eq('5');

      await updatePage.cvvInput.sendKeys('5');
      expect(await updatePage.cvvInput.getAttribute('value')).to.eq('5');

      await updatePage.dateInput.sendKeys('date');
      expect(await updatePage.dateInput.getAttribute('value')).to.match(/date/);

      // await selectLastOption(updatePage.invoiceSelect);

      expect(await updatePage.saveButton.isEnabled()).to.be.true;
      await updatePage.saveButton.click();

      await waitUntilHidden(updatePage.saveButton);
      expect(await isVisible(updatePage.saveButton)).to.be.false;

      await waitUntilCount(listPage.records, beforeRecordsCount + 1);
      expect(await listPage.records.count()).to.eq(beforeRecordsCount + 1);
    });

    describe('Details, Update, Delete flow', () => {
      after(async () => {
        const deleteButton = listPage.getDeleteButton(listPage.records.last());
        await click(deleteButton);

        deleteDialog = new CardDeleteDialog();
        await waitUntilDisplayed(deleteDialog.dialog);

        expect(await deleteDialog.title.getAttribute('id')).to.match(/saveCashBankApp.card.delete.question/);

        await click(deleteDialog.confirmButton);
        await waitUntilHidden(deleteDialog.dialog);

        expect(await isVisible(deleteDialog.dialog)).to.be.false;

        await waitUntilCount(listPage.records, beforeRecordsCount);
        expect(await listPage.records.count()).to.eq(beforeRecordsCount);
      });

      it('should load details Card page and fetch data', async () => {
        const detailsButton = listPage.getDetailsButton(listPage.records.last());
        await click(detailsButton);

        detailsPage = new CardDetailsPage();

        await waitUntilAllDisplayed([detailsPage.title, detailsPage.backButton, detailsPage.firstDetail]);

        expect(await detailsPage.title.getText()).not.to.be.empty;
        expect(await detailsPage.firstDetail.getText()).not.to.be.empty;

        await click(detailsPage.backButton);
        await waitUntilCount(listPage.records, beforeRecordsCount + 1);
      });

      it('should load edit Card page, fetch data and update', async () => {
        const editButton = listPage.getEditButton(listPage.records.last());
        await click(editButton);

        await waitUntilAllDisplayed([updatePage.title, updatePage.footer, updatePage.saveButton]);

        expect(await updatePage.title.getText()).not.to.be.empty;

        await clear(updatePage.scoreInput);
        await updatePage.scoreInput.sendKeys('6');
        expect(await updatePage.scoreInput.getAttribute('value')).to.eq('6');

        await clear(updatePage.numberInput);
        await updatePage.numberInput.sendKeys('6');
        expect(await updatePage.numberInput.getAttribute('value')).to.eq('6');

        await clear(updatePage.cvvInput);
        await updatePage.cvvInput.sendKeys('6');
        expect(await updatePage.cvvInput.getAttribute('value')).to.eq('6');

        await updatePage.dateInput.clear();
        await updatePage.dateInput.sendKeys('modified');
        expect(await updatePage.dateInput.getAttribute('value')).to.match(/modified/);

        await updatePage.saveButton.click();

        await waitUntilHidden(updatePage.saveButton);

        expect(await isVisible(updatePage.saveButton)).to.be.false;
        await waitUntilCount(listPage.records, beforeRecordsCount + 1);
      });
    });
  });
});
