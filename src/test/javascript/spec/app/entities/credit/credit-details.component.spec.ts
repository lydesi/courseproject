/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CreditDetailComponent from '@/entities/credit/credit-details.vue';
import CreditClass from '@/entities/credit/credit-details.component';
import CreditService from '@/entities/credit/credit.service';
import router from '@/router';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Credit Management Detail Component', () => {
    let wrapper: Wrapper<CreditClass>;
    let comp: CreditClass;
    let creditServiceStub: SinonStubbedInstance<CreditService>;

    beforeEach(() => {
      creditServiceStub = sinon.createStubInstance<CreditService>(CreditService);

      wrapper = shallowMount<CreditClass>(CreditDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { creditService: () => creditServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCredit = { id: 123 };
        creditServiceStub.find.resolves(foundCredit);

        // WHEN
        comp.retrieveCredit(123);
        await comp.$nextTick();

        // THEN
        expect(comp.credit).toBe(foundCredit);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCredit = { id: 123 };
        creditServiceStub.find.resolves(foundCredit);

        // WHEN
        comp.beforeRouteEnter({ params: { creditId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.credit).toBe(foundCredit);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
