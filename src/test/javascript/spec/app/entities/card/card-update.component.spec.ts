/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import CardUpdateComponent from '@/entities/card/card-update.vue';
import CardClass from '@/entities/card/card-update.component';
import CardService from '@/entities/card/card.service';

import InvoiceService from '@/entities/invoice/invoice.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Card Management Update Component', () => {
    let wrapper: Wrapper<CardClass>;
    let comp: CardClass;
    let cardServiceStub: SinonStubbedInstance<CardService>;

    beforeEach(() => {
      cardServiceStub = sinon.createStubInstance<CardService>(CardService);

      wrapper = shallowMount<CardClass>(CardUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          cardService: () => cardServiceStub,

          invoiceService: () => new InvoiceService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.card = entity;
        cardServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cardServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.card = entity;
        cardServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cardServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCard = { id: 123 };
        cardServiceStub.find.resolves(foundCard);
        cardServiceStub.retrieve.resolves([foundCard]);

        // WHEN
        comp.beforeRouteEnter({ params: { cardId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.card).toBe(foundCard);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
