import { Component, Vue, Inject } from 'vue-property-decorator';

import InvoiceService from '@/entities/invoice/invoice.service';
import { IInvoice } from '@/shared/model/invoice.model';

import { ITransaction, Transaction } from '@/shared/model/transaction.model';
import TransactionService from './transaction.service';

const validations: any = {
  transaction: {
    currencyUnit: {},
    date: {},
  },
};

@Component({
  validations,
})
export default class TransactionUpdate extends Vue {
  @Inject('transactionService') private transactionService: () => TransactionService;
  public transaction: ITransaction = new Transaction();

  @Inject('invoiceService') private invoiceService: () => InvoiceService;

  public invoices: IInvoice[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.transactionId) {
        vm.retrieveTransaction(to.params.transactionId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.transaction.id) {
      this.transactionService()
        .update(this.transaction)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('saveCashBankApp.transaction.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.transactionService()
        .create(this.transaction)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('saveCashBankApp.transaction.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveTransaction(transactionId): void {
    this.transactionService()
      .find(transactionId)
      .then(res => {
        this.transaction = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.invoiceService()
      .retrieve()
      .then(res => {
        this.invoices = res.data;
      });
  }
}
