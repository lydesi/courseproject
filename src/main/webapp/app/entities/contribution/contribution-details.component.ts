import { Component, Vue, Inject } from 'vue-property-decorator';

import { IContribution } from '@/shared/model/contribution.model';
import ContributionService from './contribution.service';

@Component
export default class ContributionDetails extends Vue {
  @Inject('contributionService') private contributionService: () => ContributionService;
  public contribution: IContribution = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.contributionId) {
        vm.retrieveContribution(to.params.contributionId);
      }
    });
  }

  public retrieveContribution(contributionId) {
    this.contributionService()
      .find(contributionId)
      .then(res => {
        this.contribution = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
