import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IContribution } from '@/shared/model/contribution.model';

import ContributionService from './contribution.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Contribution extends Vue {
  @Inject('contributionService') private contributionService: () => ContributionService;
  private removeId: number = null;

  public contributions: IContribution[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllContributions();
  }

  public clear(): void {
    this.retrieveAllContributions();
  }

  public retrieveAllContributions(): void {
    this.isFetching = true;

    this.contributionService()
      .retrieve()
      .then(
        res => {
          this.contributions = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IContribution): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeContribution(): void {
    this.contributionService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('saveCashBankApp.contribution.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllContributions();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
