import { IInvoice } from '@/shared/model/invoice.model';

export interface ICredit {
  id?: number;
  score?: number | null;
  name?: number | null;
  time?: string | null;
  currencyUnit?: string | null;
  invoice?: IInvoice | null;
}

export class Credit implements ICredit {
  constructor(
    public id?: number,
    public score?: number | null,
    public name?: number | null,
    public time?: string | null,
    public currencyUnit?: string | null,
    public invoice?: IInvoice | null
  ) {}
}
