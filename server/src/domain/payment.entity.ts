/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Invoice } from './invoice.entity';

/**
 * A Payment.
 */
@Entity('payment')
export class Payment extends BaseEntity {
    @Column({ name: 'name', nullable: true })
    name: string;

    @Column({ type: 'integer', name: 'bank_account_number', nullable: true })
    bankAccountNumber: number;

    @Column({ type: 'integer', name: 'score_number', nullable: true })
    scoreNumber: number;

    @ManyToOne((type) => Invoice)
    invoice: Invoice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
