/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Invoice } from './invoice.entity';

/**
 * A Transaction.
 */
@Entity('transaction')
export class Transaction extends BaseEntity {
    @Column({ name: 'currency_unit', nullable: true })
    currencyUnit: string;

    @Column({ name: 'date', nullable: true })
    date: string;

    @ManyToOne((type) => Invoice)
    invoice: Invoice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
