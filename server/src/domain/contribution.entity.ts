/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Invoice } from './invoice.entity';

/**
 * A Contribution.
 */
@Entity('contribution')
export class Contribution extends BaseEntity {
    @Column({ name: 'name', nullable: true })
    name: string;

    @Column({ type: 'integer', name: 'score', nullable: true })
    score: number;

    @Column({ name: 'time', nullable: true })
    time: string;

    @Column({ name: 'currency_unit', nullable: true })
    currencyUnit: string;

    @ManyToOne((type) => Invoice)
    invoice: Invoice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
