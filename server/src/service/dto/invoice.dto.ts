/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from './base.dto';

import { UserDTO } from './user.dto';

/**
 * A Invoice DTO object.
 */
export class InvoiceDTO extends BaseDTO {
    @ApiModelProperty({ description: 'score field', required: false })
    score: string;

    @ApiModelProperty({ description: 'currencyUnit field', required: false })
    currencyUnit: string;

    @ApiModelProperty({ type: UserDTO, description: 'user relationship' })
    user: UserDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
