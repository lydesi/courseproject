/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from './base.dto';

import { InvoiceDTO } from './invoice.dto';

/**
 * A Card DTO object.
 */
export class CardDTO extends BaseDTO {
    @ApiModelProperty({ description: 'score field', required: false })
    score: number;

    @ApiModelProperty({ description: 'number field', required: false })
    number: number;

    @ApiModelProperty({ description: 'cvv field', required: false })
    cvv: number;

    @ApiModelProperty({ description: 'date field', required: false })
    date: string;

    @ApiModelProperty({ type: InvoiceDTO, description: 'invoice relationship' })
    invoice: InvoiceDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
