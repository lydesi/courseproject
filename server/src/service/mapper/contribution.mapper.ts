import { Contribution } from '../../domain/contribution.entity';
import { ContributionDTO } from '../dto/contribution.dto';

/**
 * A Contribution mapper object.
 */
export class ContributionMapper {
    static fromDTOtoEntity(entityDTO: ContributionDTO): Contribution {
        if (!entityDTO) {
            return;
        }
        let entity = new Contribution();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Contribution): ContributionDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new ContributionDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
