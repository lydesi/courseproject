import { Credit } from '../../domain/credit.entity';
import { CreditDTO } from '../dto/credit.dto';

/**
 * A Credit mapper object.
 */
export class CreditMapper {
    static fromDTOtoEntity(entityDTO: CreditDTO): Credit {
        if (!entityDTO) {
            return;
        }
        let entity = new Credit();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Credit): CreditDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new CreditDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
