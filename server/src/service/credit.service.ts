import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { CreditDTO } from '../service/dto/credit.dto';
import { CreditMapper } from '../service/mapper/credit.mapper';
import { CreditRepository } from '../repository/credit.repository';

const relationshipNames = [];
relationshipNames.push('invoice');

@Injectable()
export class CreditService {
    logger = new Logger('CreditService');

    constructor(@InjectRepository(CreditRepository) private creditRepository: CreditRepository) {}

    async findById(id: string): Promise<CreditDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.creditRepository.findOne(id, options);
        return CreditMapper.fromEntityToDTO(result);
    }

    async findByfields(options: FindOneOptions<CreditDTO>): Promise<CreditDTO | undefined> {
        const result = await this.creditRepository.findOne(options);
        return CreditMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<CreditDTO>): Promise<[CreditDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.creditRepository.findAndCount(options);
        const creditDTO: CreditDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach((credit) => creditDTO.push(CreditMapper.fromEntityToDTO(credit)));
            resultList[0] = creditDTO;
        }
        return resultList;
    }

    async save(creditDTO: CreditDTO): Promise<CreditDTO | undefined> {
        const entity = CreditMapper.fromDTOtoEntity(creditDTO);
        const result = await this.creditRepository.save(entity);
        return CreditMapper.fromEntityToDTO(result);
    }

    async update(creditDTO: CreditDTO): Promise<CreditDTO | undefined> {
        const entity = CreditMapper.fromDTOtoEntity(creditDTO);
        const result = await this.creditRepository.save(entity);
        return CreditMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.creditRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
