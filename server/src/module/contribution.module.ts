import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContributionController } from '../web/rest/contribution.controller';
import { ContributionRepository } from '../repository/contribution.repository';
import { ContributionService } from '../service/contribution.service';

@Module({
    imports: [TypeOrmModule.forFeature([ContributionRepository])],
    controllers: [ContributionController],
    providers: [ContributionService],
    exports: [ContributionService],
})
export class ContributionModule {}
